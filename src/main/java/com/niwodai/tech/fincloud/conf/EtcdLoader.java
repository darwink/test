package com.niwodai.tech.fincloud.conf;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeoutException;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mousio.etcd4j.EtcdClient;
import mousio.etcd4j.responses.EtcdAuthenticationException;
import mousio.etcd4j.responses.EtcdException;

public class EtcdLoader {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EtcdLoader.class);
	
	private static final String RESOURCE_PATH = "/";
	private static final String RESOURCE_SUFFIX = "properties";
	private static final String VERSION = "version";
	
	private EtcdClient etcdClient;
	private String environment;
	private String version;
	
	public EtcdLoader(String environment, String version) {
//		etcdClient = new EtcdClient(URI.create("http://192.168.66.52:2379"));
		this.environment = environment;
		this.version = version;
	}
	
	public void loadProperties() throws IOException, EtcdException, EtcdAuthenticationException, TimeoutException {
//		etcdClient.putDir(environment).send().get();
		File rootDir = new File(getClass().getResource(RESOURCE_PATH).getPath());
		Collection<File> files = FileUtils.listFiles(rootDir, new String[]{RESOURCE_SUFFIX}, false);
		for (File file : files) {
//			etcdClient.putDir(file.getName()).send().get();
			readPropertyFile(file.getName());
			LOGGER.info(environment + File.separator + file.getName() + File.separator + VERSION);
//			etcdClient.put(environment + File.separator + file.getName() + File.separator + VERSION, version).send().get();
		}
	}

	public void readPropertyFile(String fileName) throws IOException, EtcdException, EtcdAuthenticationException, TimeoutException {
		Properties prop = new Properties();
		InputStream in = getClass().getResourceAsStream(RESOURCE_PATH + fileName);
		try {
			prop.load(in);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Set<Entry<Object, Object>> keyValue = prop.entrySet();
		for (Entry<Object, Object> entry : keyValue) {
			LOGGER.info(environment + File.separator + fileName + File.separator + String.valueOf(entry.getKey()));
//			etcdClient.put(environment + File.separator + fileName + File.separator + String.valueOf(entry.getKey()), String.valueOf(entry.getValue())).send().get();
		}
	}

	public static void main(String[] args) throws IOException, EtcdException, EtcdAuthenticationException, TimeoutException {
		EtcdLoader etcdLoader = new EtcdLoader(args[0], args[1]);
		etcdLoader.loadProperties();
	}

}
